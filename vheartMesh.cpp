#include "pch.h"
#include <opencv/cv.h>
#include <opencv/highgui.h>

static int parse_command_line(int argc, char *argv[]);
static void usage(const char *argv0);
std::string input_dir;
std::string output_file;
int number_of_images = -1;
int _facet_angle=30;
int _facet_size=3;
int _facet_distance=4;
int _cell_radius_edge_ratio=3;
int _cell_size=4;
int _show_labels = 0;


// Domain
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Labeled_image_mesh_domain_3<CGAL::Image_3,K> Mesh_domain;

// Triangulation
typedef CGAL::Mesh_triangulation_3<Mesh_domain>::type Tr;
typedef CGAL::Mesh_complex_3_in_triangulation_3<Tr> C3t3;

// Criteria
typedef CGAL::Mesh_criteria_3<Tr> Mesh_criteria;
typedef CGAL::Mesh_constant_domain_field_3<Mesh_domain::R,
                                           Mesh_domain::Index> Sizing_field;
// To avoid verbose function and named parameters call
using namespace CGAL::parameters;
using namespace std;
#define DEFAULT_DIMENSION 256

static void Tokenize(const string& str,
                     vector<string>& tokens,
                     const string& delimiters = " ");
typedef unsigned char datatype;
vector<datatype> cell_labels;
vector<double> cell_sizes;

vector<datatype> data;
vector<datatype> materials;
vector<int> mat_counts;

static void fill(const char *filename) {

    cv::Mat tmp = cv::imread(filename);
    assert (tmp.type() == cv::CV_8UC3);
    if (tmp.size() != cv::Size()) {
        cv::Mat_<cv::Vec3b>::iterator it = tmp.begin<cv::Vec3b>(),
            itEnd = tmp.end<cv::Vec3b>();
        for(; it != itEnd; ++it)
            data.push_back((datatype)(((double)(*it)[0]/255.0+(double)(*it)[1]/255.0+(double)(*it)[2]/255.0)/3.0*255));
    }
}

static void readData(std::string dirname, int num, int dim) {
    data.clear();
    for (int i=0; i<num; i++) {
        std::ostringstream strm;
        strm << i;
        std::string filename=dirname+'/'+strm.str()+".bmp";
        std::cout << "Reading file: " << filename << std::endl;
        fill(filename.c_str());
    }
}

static void show_labels(){
    for (int i=0; i<data.size(); i++){
        int mat;
        int found = 0;
        for (int j=0; j<materials.size(); j++){
            if (materials[j] == data[i]){
                mat = j;
                found = 1;
                break;
            }
        }
        if (!found){
            materials.push_back(data[i]);
            mat_counts.push_back(1);
        } else {
            mat_counts[mat]++;
        }
    }
    printf("Materials Statistics (material : count):\n");
    for (int i=0; i<materials.size(); i++){
        printf("\t %d : %d\n",materials[i],mat_counts[i]);
    }
}
int main(int argc, char* argv[]) {

    if (parse_command_line(argc,argv)){
        return -1;
    }
    // Loads image

    if (input_dir.size() == 0){
        printf("Please provide input directory\n");
        usage(argv[0]);
        return -1;
    }
    if (output_file.size() == 0){
        printf("Please provide output file name\n");
        usage(argv[0]);
        return -1;
    }
    if (number_of_images == -1){
        printf("Please specify the number of imagies to read\n");
        usage(argv[0]);
        return -1;
    }

    readData(input_dir,number_of_images,DEFAULT_DIMENSION);

    if (_show_labels) {
        show_labels();
        return 0;
    }

    CGAL::Image_3 image(_createImage(256, 256, number_of_images, 1 /*image vectoral dimension*/,
                                     1, 1, 3,/* x,y,z spacings*/
                                     sizeof(datatype) /*item size in bytes*/,
                                     WK_FIXED, SGN_UNSIGNED));

    image.set_data(&data[0]);



    // Domain
    Mesh_domain domain(image);


    Sizing_field size(_cell_size);
    for (int i=0; i<cell_labels.size(); i++){
        size.set_size(cell_sizes[i], /*volume_dimension*/3,
                      domain.index_from_subdomain_index(cell_labels[i]));
        printf("material: %d -- size: %g\n",cell_labels[i],cell_sizes[i]);
    }
    // Mesh criteria

    Mesh_criteria criteria(facet_angle=_facet_angle, facet_size=_facet_size, facet_distance=_facet_distance,
                           cell_radius_edge_ratio=_cell_radius_edge_ratio, cell_size=size);



    // Meshing
    C3t3 c3t3 = CGAL::make_mesh_3<C3t3>(domain, criteria);


    // Output
    std::ofstream medit_file(output_file.c_str());

    c3t3.output_to_medit(medit_file);

    //clean up
    image.set_data(NULL);
    medit_file.close();
    data.clear();

    return 0;
}

static int checkComplexSize(string str){
    vector<string> tokens;
    Tokenize(str, tokens, ":");
    if (tokens.size() == 1){
        return 0;
    } else {
        return 1;
    }
}

static int parseComplexSize(string str){
    vector<string> tokens;
    Tokenize(str, tokens, ",");

    for (vector<string>::iterator it = tokens.begin(); it != tokens.end(); it++){
        string substr = *it;
        vector<string> tok;
        Tokenize(substr, tok, ":");

        if (tok.size() != 2) {
            printf("Wrong Complex Size argument Format\n");
            return 1;
        }
        unsigned int material;
        double size;


        istringstream istr_mat(*tok.begin());
        istringstream istr_size(*(tok.end()-1));
        istr_mat >> material;
        istr_size >> size;

        cell_labels.push_back((datatype)material);
        cell_sizes.push_back(size);
    }
}

static int parse_command_line(int argc, char *argv[])
{
    while (1) {
        int c;
        static struct option long_options[] = {
            { "data-dir", 1, NULL, 'i' },
            { "img-num", 1, NULL, 'n' },
            { "outmesh", 1, NULL, 'o' },
            { "facet-angle", 1, NULL, 'a' },
            { "facet-distance", 1, NULL, 'd' },
            { "facet-size", 1, NULL, 's' },
            { "cell-radius-edge-ratio", 1, NULL, 'r' },
            { "cell-size", 1, NULL, 'z' },
            { "img-size", 1, NULL, 'S' },
            { "show-labels", 0, NULL, 'l' },
            { "help", 1, NULL, 'h' },
            { 0 }
        };
        c = getopt_long(argc, argv, "hi:n:o:a:d:s:r:z:S:l", long_options, NULL);

        if (c == -1)
            break;

        //std::istringstream istr((string(optarg)));
        std::istringstream istr;
        if (optarg) istr.str(string(optarg));
        switch (c) {
        case 'i':
            input_dir = string(optarg);
            break;
        case 'o':
            output_file = string(optarg);
            break;
        case 'n':
            istr >> number_of_images;
            break;
        case 'a':
            istr >> _facet_angle;
            break;
        case 'd':
            istr >> _facet_distance;
            break;
        case 's':
            istr >> _facet_size;
            break;
        case 'r':
            istr >> _cell_radius_edge_ratio;
            break;
        case 'z':
            if (!checkComplexSize(string(optarg))){
                printf("Simple Size command\n");
                istr >> _cell_size;
            } else {
                printf("Complex Size command\n");
                if (parseComplexSize(string(optarg))){
                    usage(argv[0]);
                    return 7;
                }
            }
            break;
        case 'l':
            _show_labels = 1;
            break;
        case 'S':
            break;
        case 'h':
            usage(argv[0]);
            return 7;
        default:
            usage(argv[0]);
            return 7;
        }
    }
    return 0;
}

static void usage(const char *argv0)
{
    printf("Usage:\n");
    printf("Performes tetrahedral meshing of sliced data\n\n");

    printf("%s\n",argv0);
    printf("Options:\n");
    printf("  -i, --data-dir=<dirname> \tDirectory where bmp images are stored\n");
    printf("  -n, --img-num=<number> \tHow many imagies to read\n");
    printf("  -o, --outmesh=<filename> \tSpecifies output mesh file name\n");
    printf("  -a, --facet-angle=<angle> \tfacet angle mesher parameter in degrees (default: 30)\n");
    printf("  -d, --facet-distance=<filename> \tfacet distance mesher parameter (default: 4)\n");
    printf("  -s, --facet-size=<filename> \tfacet size mesher parameter (default: 3)\n");
    printf("  -r, --cell-radius-edge-ratio=<filename> \tcell-radious-edge-ration mesher parameter (default: 3)\n");
    printf("  -z, --cell-size=<filename> \tcell size mesher parameter (default: 4)\n");
}

static void Tokenize(const string& str,
                     vector<string>& tokens,
                     const string& delimiters)
{
    // Skip delimiters at beginning.
    string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    // Find first "non-delimiter".
    string::size_type pos     = str.find_first_of(delimiters, lastPos);

    while (string::npos != pos || string::npos != lastPos)
    {
        // Found a token, add it to the vector.
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        // Skip delimiters.  Note the "not_of"
        lastPos = str.find_first_not_of(delimiters, pos);
        // Find next "non-delimiter"
        pos = str.find_first_of(delimiters, lastPos);
    }
}
